import com.google.gson.annotations.SerializedName

class Course {
    var id: Int = 0
    @SerializedName("title")
    lateinit var title: String
    @SerializedName("summary")
    lateinit var description: String
    @SerializedName("learners_count")
    var learnersCount: Int = 0
}

class CoursesContainer {
    lateinit var courses: List<Course>
    lateinit var meta: Map<*, *>
}
